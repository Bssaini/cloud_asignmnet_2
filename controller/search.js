/**
 * Auther Bhupinder Saini
         ID. n8983631
 */
'use strict';

const config = require('../config/config.js');

var app = require('../app');
var http = require('http');
var Twit = require('twit');
var natural = require('natural');
var sentiment = require('sentiment');
var WordPOS = require('wordpos'),
    wordpos = new WordPOS();
var tokenizer = new natural.WordTokenizer();
var wordnet = new natural.WordNet();
var request = require('request');
var entityExtraction =  require('haven-entity-extraction');        
var ip = require('ip');
const requestIp = require('request-ip');

// Import model
var NewsModel = require('../model/model');
var myMap = new Map();


exports.index = function(req, res){    
    var myIp = ip.address();
    const clientIp = requestIp.getClientIp(req); 
    var serverIp = clientIp.replace('::ffff:', '');
    res.render('search', {title:'Tweet', ip:serverIp});
}

exports.getTwit = function(client, keydata){
    //Initiate value
    var positiveCount, negativeCount,noSentimentCount,  tokens = [], topTokens = [];
    removeDBContent(client.id);
    //Set twit access token
    var T = new Twit({
        consumer_key: config.consumer_key,
        consumer_secret: config.consumer_secret,
        access_token: config.access_token_key,
        access_token_secret: config.access_token_secret
    });

    //Natural node module.

    //Get keywords: Tokenize, stemming.
    var hashTagData = [];    
    var totalWords = keydata.split(" ");
    for(var i = 0; i < totalWords.length; i++){
        if(totalWords[i].includes("#")) hashTagData.push(tokenizer.tokenize(totalWords[i])[0]);
    }

    var tokenizerData = tokenizer.tokenize(keydata);
    var tokenizerKeyData = [];
    for(var i = 0; i < tokenizerData.length; i++){
        tokenizerKeyData.push(/*natural.PorterStemmer.stem*/(tokenizerData[i].toLocaleLowerCase()));
    }

    var stream = T.stream('statuses/filter', { track: [tokenizerKeyData], language: 'en' });
    //stream = T.stream('statuses/filter', { track: [keydata], language: 'en' });
    myMap.set(client.id, stream);
    //Initiate positive/negative count
    positiveCount = negativeCount = noSentimentCount = 0;
    //Initiate tokens(Used words)
    tokens = [];
    topTokens = [];
    var topTokenCount = [];
    for(var i =0; i < 11; i++){
        topTokenCount[i] = 0;
        topTokens[i] = null;
    }

    stream.on('tweet', function (tweet) {    
        //Filter the incoming tweet                      
        //Filter by hashtag
        var tweetHashTags = [];
        for(var i = 0; i < tweet.entities.hashtags.length; i++){
            tweetHashTags.push(tweet.entities.hashtags[i].text);
        }
        for(var i = 0; i < hashTagData.length; i++){
            for(var j = 0; j < tweetHashTags.length; j++){
                if(tweetHashTags[j].toLowerCase().indexOf(hashTagData[i].toLowerCase()) !== -1) break;
            }
            if(j == tweetHashTags.length) return;
        }
        //Filter by keywords  
        var Trie = natural.Trie;
        var trie = new Trie();
        var tweetText = tweet.text;
        var tokenizerTweetTextData = tokenizer.tokenize(tweetText);
        var stemTweetTextData = [];
        for(var j = 0; j < tokenizerTweetTextData.length; j++){
            stemTweetTextData.push(/*natural.PorterStemmer.stem*/(tokenizerTweetTextData[j].toLowerCase()));
        }
        trie.addStrings(stemTweetTextData);        

        for(var i = 0; i < tokenizerKeyData.length; i++){
            if(!trie.contains(tokenizerKeyData[i])) return;
        }

        //Sentiment analysis of tweet
        var tweetSentiment = sentiment(tweet.text);        

        //get token of sentiment
        var sentimentTokens = tweetSentiment.tokens;
        
        var havenKey = config.haven_Key;
        
        var text = tweet.text;
        entityExtraction(text, havenKey, function(results){
            /*
            console.log('---------------------------');
            console.log(text);
            console.log(results); // Do some awesome stuff with results
            */
            saveTweetEntities(results, keydata, client.id)
        });

        //Save Tweet data to DB
        saveTweetData(tweet, keydata, tweetSentiment, client.id);

        for(var i = 0; i < sentimentTokens.length; i++){
            
            //Stem the tokens before save
            if(tokens[natural.PorterStemmer.stem(sentimentTokens[i])]){
                tokens[sentimentTokens[i]]++;                
            }
            else{
                tokens[sentimentTokens[i]] = 1;
            }

            //Get the top 10 frequent words using with keyword
            var replacePos = 20;
            topTokens[replacePos] = sentimentTokens[i];
            for(var j = 0; j <= replacePos; j++){
                if(topTokens[j] == null){
                    topTokens[j] = sentimentTokens[i];
                    replacePos = j;
                    break;
                }
                if(sentimentTokens[i] == topTokens[j]){
                    replacePos = j;
                    break;
                }
            }

            for(var j = replacePos; j > 0; j--){
                if(tokens[topTokens[j]] > tokens[topTokens[j-1]]){
                    var temp = topTokens[j];                    
                    topTokens[j] = topTokens[j-1];
                    topTokens[j-1] = temp;
                }
            }
        }

        //set tranfer data through socket
        tweet.comparative = tweetSentiment.comparative;
        tweet.topTokens = topTokens;

        if(tweetSentiment.comparative > 0){
            positiveCount++;
            tweet.positiveCount = positiveCount;
            tweet.negativeCount = negativeCount;
            tweet.noSentimentCount = noSentimentCount;
            client.emit('posTwit', tweet);            
        }
        else if(tweetSentiment.comparative < 0){
            negativeCount++;
            tweet.positiveCount = positiveCount;
            tweet.negativeCount = negativeCount;
            tweet.noSentimentCount = noSentimentCount;
            client.emit('negativeTwit', tweet);
        }
        else{
            noSentimentCount++;
            tweet.positiveCount = positiveCount;
            tweet.negativeCount = negativeCount;
            tweet.noSentimentCount = noSentimentCount;
            client.emit('noSentimentTwit', tweet);
        }
    });

    stream.on('disconnect', function (disconnectMessage) {
        //...
        console.log("The connection with server was closed. Try again.");
        client.emit('disconnect');
    })
}

//This function is called when user turn off live stream.
exports.cancelTwit = function(client){
    var stream = myMap.get(client.id);
    if(stream)
        stream.stop();
    console.log("Twitter Stream stopped.");
}

//This function is called when user change keyword or close browser.
exports.closeTwit = function(client){
    var stream = myMap.get(client.id);
    if(stream)
        stream.stop();
    console.log("Twitter Stream closed.");
}

//This function is called when user turn on live stream.
exports.resumeTwit = function(client){
    var stream = myMap.get(client.id);
    if(stream){
        stream.start();
        myMap.delete(client.id);
    }        
    console.log("Twitter Stream resumed.");     
}

exports.getFilters = function(req, res){
    var keyword = req.body.keyword;
    //console.log(keyword);
    var tokenizer = new natural.WordTokenizer();
    var tokenizerKeyWords = tokenizer.tokenize(keyword);
    res.json({filters:tokenizerKeyWords});
}

exports.removeDB = function(client){
    removeDBContent(client.id);
}

function removeDBContent(socketId){    
    NewsModel.remove({socketId:socketId}, function(err){
        if(err) console.log(err);
    })
}

function saveTweetEntities(results, keydata, socketId){
    console.log(results);
    if(!results) return;
    if(results.people_eng){
        console.log(results.people_eng);
        for(var i = 0; i < results.people_eng.length; i++){
            savePeopleData(results.people_eng[i].normalized_text, keydata, socketId);
        }
    }
    if(results.person_name_component_eng){
        console.log(results.person_name_component_eng);
        for(var i = 0; i < results.person_name_component_eng.length; i++){
            savePeopleData(results.person_name_component_eng[i].normalized_text, keydata, socketId);
        }
    }
    if(results.place_eng){
        console.log(results.place_eng);
        for(var i = 0; i < results.place_eng[i].length; i++){
            savePlaceData(results.place_eng[i].normalized_text, keydata, socketId);
        }
    }
    if(results.companies_eng){
        console.log(results.companies_eng);
        for(var i = 0; i < results.companies_eng[i].length; i++){
            saveCompanyData(results.companies_eng[i].normalized_text, keydata, socketId);
        }
    }
    if(results.organizations){
        console.log(results.organizations);
        for(var i = 0; i < results.organizations[i].length; i++){
            saveOrganizationData(results.organizations[i].normalized_text, keydata, socketId);
        }
    }
    if(results.teams){
        console.log(results.teams);
        for(var i = 0; i < results.teams.length; i++){
            saveTeamData(results.teams[i].normalized_text, keydata, socketId);
        }
    }
}

function saveTweetData(tweet, keydata, tweetSentiment, socketId){
    //Save PlaceData
    if(tweet.place != null) {
        savePlaceData(tweet.place, keydata, socketId);
    }
    //Save HashData
    if(tweet.entities.hashtags != null){
        for(var i = 0; i < tweet.entities.hashtags.length; i++){
            saveHashTagData(tweet.entities.hashtags[i].text, keydata, socketId);
        }
    }
    //Save Sentiment Words
    if(tweetSentiment.words != null){
        for(var i = 0; i < tweetSentiment.words.length; i++){
            saveSentimentWords(tweetSentiment.words[i], keydata, socketId);
        }
    }
    //console.log(tweetSentiment);
}

//Save Sentiment words
function saveSentimentWords(sentimentWord, keydata, socketId){
    NewsModel.findOneAndUpdate(
        {socketId: socketId, keyword:keydata},
        {$addToSet:{"sentimentWords":sentimentWord}},
        {safe: true, upsert: true, new: true},
        function(err, model){
            if(err) console.log(err);
            return;
        }
    )
}
//Save HashData
function saveHashTagData(hashTags, keydata, socketId){
    NewsModel.findOneAndUpdate(
        {socketId: socketId, keyword:keydata},
        {$addToSet:{"hashData":hashTags}},
        {safe: true, upsert: true, new: true},
        function(err, model){
            if(err) console.log(err);
            return;
        }
    )
}

//Save PeopleData
function savePeopleData(peopleName, keydata, socketId){
    NewsModel.findOneAndUpdate(
        {socketId:socketId, keyword:keydata},
        {$addToSet:{"peopleData":peopleName}},
        function(err, model){
            if(err) console.log(err)
            return;
        }
    )
}

//Save CompanyData
function saveCompanyData(companyName, keydata, socketId){
    NewsModel.findOneAndUpdate(
        {socketId:socketId, keyword:keydata},
        {$addToSet:{"companyData":companyName}},
        function(err, model){
            if(err) console.log(err);
            return;
        }
    )
}

//Save OrganizationData
function saveOrganizationData(organizationName, keydata, socketId){
    NewsModel.fineOneAndUpdate(
        {socketId:socketId, keyword:keydata},
        {$addToSet:{"organizaitonData":organizationName}},
        function(err, model){
            if(err) console.log(err);
            return;
        }
    )
}

//Save TeamData
function saveTeamData(teamName, keydata, socketId){
    NewsModel.findOneAndUpdate(
        {socketId:socketId, keyword:keydata},
        {$addToSet:{"teamData":teamName}},
        function(err, model){
            if(err) console.log(err);
            return;
        }
    )
}

//Save PlaceData
function savePlaceData(place, keydata, socketId){
    NewsModel.findOneAndUpdate(
        {socketId: socketId, keyword:keydata},
        {$addToSet:{"location":place.name}},
        function(err, model){
            if(err) console.log(err);
            return;
        }
    );
}

exports.getMoreData = function(req, res){
    var socketId = req.body.socketId;
    NewsModel.find({socketId:socketId}).exec(function(err, data){
        if(err)
            return console.log(err);
        res.json({data:data});
    })
}