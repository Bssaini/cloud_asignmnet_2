/**
 * Auther Bhupinder Saini
         ID. n8983631
 */

 /* Db model */
 
var mongoose = require("mongoose");
var Schema = mongoose.Schema;


var newsSchema = new Schema({
    socketId:'string',
    keyword:'string',
    sentimentWords:['string'],
    location:['string'],
    hashData:['string'],
    peopleData:['string'],
    companyData:['string'],
    organizaionData:['string'],
    teamData:['string']
});

module.exports = mongoose.model('NewsModel', newsSchema);