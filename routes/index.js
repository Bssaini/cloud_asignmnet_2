
/**
 * Auther Bhupinder Saini
         ID. n8983631
 */

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/search');
  //res.render('index', { title: 'Express' });
});

module.exports = router;
