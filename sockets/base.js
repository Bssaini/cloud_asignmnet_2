/**
 * Auther Bhupinder Saini
         ID. n8983631
 */

var search = require('../controller/search.js');

module.exports = function(io){
    io.on('connection', function(client) {  
        console.log('Client connected...');
      
        client.on('join', function(data) {
            if(data == "disconnect"){
                search.cancelTwit(this);
                console.log("disconnect");
            }
            else if(data == "resume"){
              search.resumeTwit(this);
              console.log("Resume");
            }
            else{
                search.cancelTwit(this);
                search.getTwit(this, data);
                console.log("join");
            }      
        });
      
        client.on('messages', function(data) {
            //client.emit('broad', data);
            //client.broadcast.emit('broad',data);
        });
        client.on('disconnect', function(){
          search.closeTwit(this);
          search.removeDB(this);
        });
    });
}